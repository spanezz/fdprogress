# fdprogress

Print progress indicators for programs that read files sequentially.

fdprogress monitors file descriptor offsets and prints progressbars comparing
them to file sizes.

Pattern can be any [glob expression](https://docs.python.org/3.5/library/fnmatch.html).

```
usage: fdprogress [-h] [--verbose] [--debug] [--pid PID] [pattern]

show progress from file descriptor offsets

positional arguments:
  pattern            file name to monitor

optional arguments:
  -h, --help         show this help message and exit
  --verbose, -v      verbose output
  --debug            debug output
  --pid PID, -p PID  PID of process to monitor
```

## pv

[pv](https://ivarch.com/programs/pv.shtml) has a `--watchfd` option that does
most of what fdprogress is trying to do: use that instead.

## fivi

[fivi](https://github.com/youam/fivi) also exists, with specific features to
show progressbars for filter commands.
